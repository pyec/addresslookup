<!DOCTYPE html>
<html lang="en">

	<head>
	
		<title>Address Lookup | Halifax</title>
		
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/styles/app.css">

		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto+Condensed'>
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300'>
		
		<script src="<?= base_url() ?>_assets/plugins/jquery/jquery.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?= base_url() ?>_assets/plugins/placeholders/placeholders.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
		
	</head>
	
	<body>
		<a name="back_to_top"></a>
		
		<div class="navbar navbar-inverse navbar-fixed-top">
	        <div class="container">
	         	<div class="navbar-header">
					<?php if($this->session->userdata('UserName')): ?>
					<a type="button" class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars" style="color:white"></i></a>
					<?php endif ?>
					<span class="navbar-brand" href="<?= base_url() ?>">
						<div class="logo pull-left">Address <span>Lookup</span></div>
					</span>
				</div>
				<?php if($this->session->userdata('UserName')): ?>
				<div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?= base_url() ?>main/logout">Logout <?= $this->session->userdata('UserNameShort') ?></a></li>
						</ul>
					</div>
				</div>
				<?php endif ?>
				<!--/.nav-collapse -->
	        </div>
	    </div>