<!-- Page -->
<div id="page" style="padding-top:100px;">
	<div class="container">

		<!-- <link rel="stylesheet" type="text/css" href="http://ws1.postescanada-canadapost.ca/css/addresscomplete-2.20.min.css?key=zd92-pw46-jn55-ka95" /><script type="text/javascript" src="http://ws1.postescanada-canadapost.ca/js/addresscomplete-2.20.min.js?key=zd92-pw46-jn55-ka95"></script> -->
		<!--<link rel="stylesheet" type="text/css" href="http://ws1.postescanada-canadapost.ca/css/addresscomplete-2.20.min.css?key=zn49-fr45-hc26-fg95" /><script type="text/javascript" src="http://ws1.postescanada-canadapost.ca/js/addresscomplete-2.20.min.js?key=zn49-fr45-hc26-fg95"></script> -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>_assets/styles/addresscomplete-2.20.min.css" /><script type="text/javascript" src="<?= base_url() ?>_assets/scripts/addresscomplete-2.20.min.js"></script>

		<form role="form">

			<div class="col-sm-6 col-sm-offset-3">

				<div class="row">
					<div class="col-sm-12">
						<h2 class="text-center">Search</h2>
						<div class="form-group">
							<label for="address" class="sr-only">Search</label>
							<input type="text" class="form-control input-lg text-center" name="address" id="address" placeholder="Enter a street address or postal code" autofocus>
						</div>
					</div>
				</div>

				<!-- <div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="formatted" class="sr-only">Confirmed Address</label>
							<textarea class="form-control input-lg" name="formatted" rows="5"></textarea>
						</div>
					</div>
				</div> -->
				
				<h3 id="addressContainer" style="display:none;text-align:center;">
			        <span id="HeaderAddressLabel">
			        </span>
			    </h3>

				<!-- <script type="text/javascript" src="//www.canadapost.ca/pca/Scripts/s_code.js"></script> -->
                <script type="text/javascript" src="<?= base_url() ?>_assets/scripts/s_code.js"></script>
			    <script type="text/javascript">
			        addressComplete.listen('load', function(control) {
			            control.listen("populate", function(address) {

			                var label = address.Label;

			                document.getElementById('HeaderAddressLabel').innerHTML = label.replace(/(\r\n|\n\n|\n|\r)/gm, '<br />');

			                // document.getElementById('finder-error').style.display = 'none';
			                $('#addressContainer').slideDown();
			                $('#address').attr({"placeholder":"Enter a street address or postal code"}).val('');
			            });

			            // control.listen("error", function (error) {
			            //     control.hide();
			            //     $('#finder-error .message').html(error == "Request not allowed from this IP" ? "<b>Sorry, you have used up your free lookups for today.</b></br>If you would like to add AddressComplete to your website, sign up for our free trial now." : "Sorry, we couldn't find any results. Please try again.");
			            //     $('#finder-error').slideDown('fast');
			            // });
			        });
			    </script>

			</div>

			<!-- <button type="submit" class="btn btn-primary">Submit</button> -->
		</form>

		<?php //print_r($this->session->all_userdata()) ?>

	</div>
</div>
