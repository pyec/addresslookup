<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*           
============================================================================
 Class: 		Deliverables
 Description:	Main application controller. 
 				Interacts with the deliverables_model and application views.
============================================================================
*/
class Main extends CI_Controller {
/*           
============================================================================
 index
----------------------------------------------------------------------------
 Displays all deliverables for a particular filter
============================================================================
*/
	public function index()
	{
		// check for session
		if($this->session->userdata('UserName'))
		{
			// // check which view type to serve
			// if($filter['ViewType'] == 2) // focus area view
			// {
			// 	$data['page'] = "pages/focus-area-results";
			// }
			// elseif($filter['ViewType'] == 3) // risk view
			// {
			// 	$data['page'] = "pages/risk-results";
			// }
			// elseif($filter['ViewType'] == 4) // business unit view
			// {
			// 	$data['page'] = "pages/business-unit-results";
			// }
			// else // default view // $filter['ViewType'] == 1
			// {
				$data['page'] = "pages/index";			
			// }

			$data['page'] = "pages/index";
			$this->load->view('template/master', $data);
			// $this->session->sess_destroy();

		}
		else $this->load->view('pages/login');
	}

/*-----------------------------------------------------------------------------------
	Validate check pass the credentials from the login form against the LDAP server,
	if valid, an array of user information will be returned to be stored in session.
	if not, we return the user to the login screen with an error message.
------------------------------------------------------------------------------------*/
	public function authenticate()
	{
		// Get the user credentials though post
		$credentials = $this->input->post(null, true);

		// modify they user name to add the domain so the user doesn't have to type it.
		$user_name = $credentials['user_name'];

		// HRP
		if (strpos($user_name, 'hrp\\') !== false)
		{
			$user_name = str_replace("hrp\\", "", $user_name);
			
			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = '71.7.141.101'; // msrodc11.hrp.halifax.ca
				$base = 'OU=HRP Users,OU=Halifax Regional Police,DC=hrp,DC=halifax,DC=ca';
			}
			// dev environment
			else
			{
				$host_name = 'msaddns05.hrp.halifax.ca'; 	// IP: 172.30.110.95
				$base = 'DC=hrp,DC=halifax,DC=ca';
			}
			 				
		} // HRM & Library
		elseif (strpos($user_name, 'hrm\\') !== false)
		{
			$user_name = str_replace("hrm\\", "", $user_name);

			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				//$host_name = '71.7.141.100'; // msrodc01.hrm.halifax.ca
				//$base = 'DC=hrm,DC=halifax,DC=ca';
				$host_name = 'MSDC05.hrm.halifax.ca'; 	// IP: 172.25.210.95
				$base = 'OU=HRM Users,OU=Halifax Regional Municipality,DC=hrm,DC=halifax,DC=ca'; 
			}
			// dev environment
			else
			{
				$host_name = 'MSDC05.hrm.halifax.ca'; 	// IP: 172.25.210.95
				$base = 'OU=HRM Users,OU=Halifax Regional Municipality,DC=hrm,DC=halifax,DC=ca'; 
			}

		}
		
		// Set up the config array for the LDAP library
		$config = array(
			'port' 		=> '389', // 389 = standard | 636 = secure
			'host_name' => $host_name, 
			'base' 		=> $base, 
			'rdn' 		=> $credentials['user_name'], 
			'password' 	=> $credentials['password'], 
		);

		// print_r($config);

		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		// Call the validate user function and pass it the filter, in this instance we want to filter by account name (i.e. the user name the typed in the login form)
		$user_data = $this->ldap->validate_user("(samaccountname=".$user_name.")");

		// print_r($user_data);

		// if the user is not validated.
		// if (!$user_data)
		// {
		// 	$this->session->set_flashdata('error', 'Invalid credentials.');
		// 	redirect('login');
		// }
		
		// $user_data['rdn'] = $credentials['user_name'];
	
		// // Other wise store the user data in session and redirect the user to the home controller,
		// // with the session set the user will now gain access to the web site.
		// $user_data['user'] = FALSE;
		// $user_data['admin'] = FALSE;

		// for ($i=0; $i < $user_data['groups']['count']; $i++) 
		// { 
		// 	if (strpos($user_data['groups'][$i], 'AppAccountabilityReportingAdmin') !== false)
		// 	{
		// 		$user_data['user'] = TRUE;
		// 		$user_data['admin'] = TRUE;
		// 		break; // exit for loop
		// 	}
		// 	if (strpos($user_data['groups'][$i],'AppAccountabilityReporting') !== false) 
		// 	{
		// 		$user_data['user'] = TRUE;
		// 		$business_units = $this->main_model->get_business_units();
		// 		foreach ($business_units as $business_unit)
		// 		{
		// 			if (strpos($user_data['groups'][$i], 'AppAccountabilityReporting'.$business_unit['BusinessUnitCode']) !== false)
		// 			{
		// 				$user_data['division'] = $business_unit['BusinessUnitCode'];
		// 				break; // exit for loop
		// 			}
		// 		}
		// 	}
		// }

		// // user doesn't have pemission
		// if (!$user_data['user'])
		// {
		// 	$this->session->set_flashdata('error', 'You do not have permission access to this system. Contact the Service Desk for support.');
		// 	redirect('login');
		// }

		if (!$user_data)
		{
			$this->session->set_flashdata('error', 'Invalid credentials.');
			redirect('main/login');
		}
		else
		{
			$data = array(
				// 'UserID'				=> $user_data['user']['UserID'],
				'UserName'  			=> $user_data['first_name']." ".$user_data['last_name'],
				'UserNameShort' 		=> $user_data['first_name'],
				'UserEmail'    			=> $user_data['email_address'],
				// 'UserBusinessUnitCode'  => $user_data['division'],
				// 'UserBusinessUnitID' 	=> $this->main_model->get_business_unit_id($user_data['division']),
				// 'UserAdminFlag'			=> $user_data['admin'],
				// 'UserFlag'				=> $user_data['user'],
				'logged_in' 			=> TRUE
			);

			// print_r($data);
			// die('end');

			$this->session->set_userdata($data);
		}

	



		// DEBUG VIA EMAIL
		//=============================================
		// $output = "<html><body>\n";
		// $output .=  "<p>".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."</p>";
		// $output .=  "<h1>".$user_name."</h1>";

		// // User array
		// $output .=  "<h2>User Data Array</h2>";
		// foreach ($user_data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }

		// // Data array
		// $output .=  "<h2>Data Array</h2>";
		// foreach ($data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }
		// // print_r($this->session->all_userdata());
		// $output .= "</body></html>";

		// $config['charset']  = 'utf-8';			// added
		// $config['mailtype'] = 'html';
		// $this->email->set_newline( "\r\n" );	// added
		// $this->email->set_crlf( "\r\n" );		// added
		// $this->email->initialize($config);
		// $this->email->clear();					// added

		// $this->email->from('no-reply@halifax.ca', 'Halifax Regional Municipality');
		// $this->email->to('pompilg@halifax.ca'); 
		// $this->email->subject('Accountability Debuging');
		// $this->email->message($output);	
		// $this->email->send();
		//=============================================


		redirect('');	// loads deliverables() (default function), pointing to root keeps the url clean
	}

/*           
============================================================================
 login
----------------------------------------------------------------------------
 Checks POST data for a valid email and password.
----------------------------------------------------------------------------
 On success: sets session and redirects back to index to display a list of 
 all strategic deliverables.
 On fail: return to login screen.
============================================================================
*/
	public function login()
	{	
		$data['page'] = "pages/login";
		$this->load->view('template/master', $data);
	}
/*           
============================================================================
 logout
----------------------------------------------------------------------------
 Destroys session and returns user to the login screen
============================================================================
*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('', 'refresh');
	}

}